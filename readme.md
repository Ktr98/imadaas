### The `imad_web_page` container

the `imad_web_page` container is just the container which setup all the librairies required for the differents actions of the Imad Process
It's based on python2 and some actions like `clustering` doesn't need it.


## Deployment of the 1st action: Web Page Collection

wsk -i action create web\_page\_collection --docker nivekiba/imad\_web\_page web\_page\_collection_action.py

- params

```json
{
	app_name: "nom de l'app"
}
```

- return

```json
{
	corpus: "le corpus qui recense les documents trouvés sur l'application",
	app_name: "nom de l'app"
}
```

where `web_page_collection_action.py` is the concatenation of the `imad_text_search_functions.py` file and the `web_page_collection.py`/`__main__.py` file


## Deployment of the 2nd action: Clustering

wsk -i action create clustering clustering.py

- params

```json
{
	corpus: "le corpus qui recense les documents trouvés sur l'application",
	app_name: "nom de l'app"
}
```

- return

```json
{
	"corpus": corpus,
	"keywords": 'les mots clés',
	"clustering": 'un objet contenant les differents clusters faits',
	"app_name": "nom d'app"
}
```

## Deployment of the 3rd action: Clusters_Analysis

wsk -i action create clusters\_analysis --docker nivekiba/imad\_web\_page clusters\_analysis\_action.py

- params

```json
{
	"corpus": corpus,
	"keywords": 'les mots clés',
	"clustering": 'un objet contenant les differents clusters faits',
	"app_name": "nom d'app"
}
```

- return

```json
{
	"corpus": corpus,
	"keywords": 'les mots clés',
	"analyse": 'un objet contenant les differents clusters ainsi que leurs scores et les entités présentes',
	"app_name": "nom d'app"
}
```

## Deployment of the 4th action: Docs_Selection

wsk -i action create docs\_selection --docker nivekiba/imad\_web\_page docs\_selection\_action.py

- param

```json
{
	"corpus": corpus,
	"keywords": 'les mots clés',
	"analyse": 'un objet contenant les differents clusters ainsi que leurs scores et les entités présentes',
	"app_name": "nom d'app"
}
```

- return

```json
{
	"corpus": corpus,
	"selection": 'une selection des entités ainsi que leur dictionnaire présent dans chaque cluster'
}
```

## Deployment of the 5th action: Trusted_Entities_Extraction

- param

```json
{
	"corpus": corpus,
	"selection": 'une selection des entités ainsi que leur dictionnaire présent dans chaque cluster'
}
```

- return

```json{
	"nom de l'entité la plus proche de l'app_name": "score de cette entité"
}
```

import imad_text_search_functions as itsf
from urllib import unquote
from re import sub

MOTEUR = "GOOGLE"
NB_ENTITES = 10
SEUIL_PHONETIQUE = 75
NB_SYLLABES_MIN = 2

def main(args):

    app_name = args.get("app_name")
    requete = app_name+"+site:en.wikipedia.org"
    json = itsf.requete_moteur(requete, MOTEUR)
    items = json['items']

    # Calculer les scores
    i = 0
    score = list()
    entities = list()
    for i in range(0, NB_ENTITES) :
        nom = unquote(items[i]['link'])
        nom = sub(r".*\/", "", nom)
        nom = nom.replace("_", " ")
        entity = itsf.build_entity(nom, None, 'other')
        score.append(itsf.calculer_score(entity, SEUIL_PHONETIQUE, NB_SYLLABES_MIN, app_name))
        entities.append(entity)

    # Ne garder que le meilleur score
    Max = 0
    imax = -1
    for i in range(0, NB_ENTITES) :
        if (score[i] > Max) :
            Max = score[i]
            imax = i

    # Afficher le resultat
    if (imax == -1) :
        print "\nPas d'entite de confiance trouvee.."
    else :
        print "\nCorrespondances detectees avec :"
        for i in range(0, NB_ENTITES) :
            if (score[i] > 0) :
                print "\n- " + entities[i]['name'] + " (" + score[i] + ")"

    # valeur a retourner
    final_result = list()
    for i in range(0, NB_ENTITES) :
        if (score[i] > 0) :
            final_result.append({entities[i]['name']: score[i]})
    
    return {"result" : final_result}
    

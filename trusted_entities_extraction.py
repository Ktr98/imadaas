import requests
import re
from bs4 import BeautifulSoup

def http_get(link):
	return requests.get(link).text

def extract_domain(corpus, selection):
	ext = {"scores": 0, "company": 0}
	ext["scores"] = dict()
	
	selected = selection["selected"]
	for i in selected:
		if selection["entities"][i] != '':
			nom = selection["entities"][i]["name"]
			if nom in ext["scores"]: ext["scores"][nom] += 1
			else: ext["scores"][nom] = 1
	ext["company"] = ''
	score_max = 0
	for c in ext["scores"]:
		s = ext["scores"][c]
		if s > score_max:
			score_max = s
			ext["company"] = c
			
	return ext

def extract_meta(corpus, selection):
	ext = {}
	ext["scores"] = dict()
	
	selected = selection["selected"]
	for i in selected:
		link = corpus["links"][i]
		content = http_get(link)
		soup = BeautifulSoup(content)
		metas = soup.find_all("meta")
		for meta in metas:
			if meta.get('name') == 'author' and meta.get('content') != '':
				auteur = meta.get('content')
				if auteur in ext["scores"]:
					ext["scores"][auteur] += 1
				else: ext["scores"][auteur] = 1
	
	ext['company'] = ''
	score_max = 0
	for c in ext["scores"]:
		s = ext["scores"][c]
		if s > score_max:
			score_max = s
			ext['company'] = c
	return ext

def calculer_fenetres(content):
	result = []
	content = content.replace("&copy;", "©")
	content = content.lower().replace("copyrights", "©")
	# remove all tags (strip_tags)
	cleanr = re.compile("<.*?>")
	content = re.sub(cleanr, ' ', content)
	# end removing
	fenetres = re.findall("(.*©.*)", content)
	
	for echantillon in fenetres:
		echantillon = " ".join(echantillon.split())
		result.append(echantillon)
	return result
	
def trouver_nom(fenetre):
	result = ''
	regex = [
		'© ?20\d\d ?- ?20\d\d (.+)\.',
		'© ?20\d\d (.+)\.',
		'© ?(.+)\.',
		'© ?20\d\d ?- ?20\d\d (.+)$',
		'© ?20\d\d (.+)$',
		'© ?(.+) ?-? 20\d\d'
	]
	for reg in regex:
		res = re.findall(reg, fenetre)
		if len(res) > 1: 
			return res[0]
	return result

def extract_copyright(corpus, selection):
	ext = {}
	ext["scores"] = dict()
	
	selected = selection["selected"]
	for i in selected:
		link = corpus["links"][i]
		content = http_get(link)
		fenetres = calculer_fenetres(content)
		for fenetre in fenetres:
			name = trouver_nom(fenetre)
			if name != '':
				if name in ext["scores"]: ext["scores"][name] += 1
				else: ext["scores"][name] = 1
	
	ext['company'] = ''
	score_max = 0
	for c in ext["scores"]:
		s = ext["scores"][c]
		if s > score_max:
			score_max = s
			ext['company'] = c
	return ext

def extract(corpus, selection):
	extraction = {"scores": 0, "company": 0}
	ex1 = extract_domain(corpus, selection)
	ex2 = extract_meta(corpus, selection)
	ex3 = extract_copyright(corpus, selection)
	extractions = [ex1, ex2, ex3]
	
	extraction["scores"] = dict()
	for ex in extractions:
		for key in ex["scores"]:
			val = ex["scores"][key]
			key = key.lower().lstrip().rstrip()
			if key in extraction["scores"]: extraction["scores"][key] += val
			else: extraction["scores"][key] = val
	
	extraction['company'] = ''
	score_max = 0
	for c in extraction["scores"]:
		s = extraction["scores"][c]
		if s > score_max:
			score_max = s
			extraction['company'] = c
			
	return extraction

obj = "{'corpus': {'titres': [['facebook', 'log', 'sign'], ['log', 'facebook', 'facebook'], ['facebook', 'developers'], ['facebook', 'ads', 'online', 'advertising', 'facebook', 'facebook'], ['log', 'facebook', 'facebook'], ['log', 'facebook', 'facebook'], ['facebook', 'log', 'sign'], ['facebook'], ['facebook', 'analytics', 'drive', 'growth', 'web', 'mobile'], ['facebook', 'log', 'sign'], ['facebook', 'linkedin'], ['top', 'facebook', 'statistics', 'updated', 'april'], ['smart', 'video', 'calling', 'alexa', 'built-in', 'portal', 'facebook']], 'documents': [['facebook', 'log', 'sign', 'create', 'account', 'log', 'facebook', 'connect', 'friends', 'family', 'people', 'share', 'photos', 'videos', 'send', 'messages', 'updates'], ['log', 'facebook', 'facebook', 'log', 'facebook', 'start', 'sharing', 'connecting', 'friends', 'family', 'people'], ['facebook', 'developers', 'code', 'connect', 'people', 'facebook', 'developers', 'explore', 'ai', 'business', 'tools', 'gaming', 'open', 'source', 'publishing', 'social', 'hardware', 'social', 'integration'], ['facebook', 'ads', 'online', 'advertising', 'facebook', 'facebook', 'facebook', 'efficient', 'ways', 'advertise', 'online', 'connect', 'businesses', 'people', 'device', 'facebook', 'marketing'], ['log', 'facebook', 'facebook', 'log', 'facebook', 'start', 'sharing', 'connecting', 'friends', 'family', 'people'], ['log', 'facebook', 'facebook', 'log', 'facebook', 'start', 'sharing', 'connecting', 'friends', 'family', 'people'], ['facebook', 'log', 'sign', 'create', 'account', 'log', 'facebook', 'connect', 'friends', 'family', 'people', 'share', 'photos', 'videos', 'send', 'messages', 'updates'], ['facebook', 'messenger', 'instantly', 'connect', 'people', 'life', 'sign', 'facebook', 'started', 'continue', 'signed', 'facebook'], ['facebook', 'analytics', 'drive', 'growth', 'web', 'mobile', 'facebook', 'analytics', 'tool', 'insights', 'business', 'understand', 'customers', 'journey', 'mobile', 'web', 'optimize', 'growth'], ['facebook', 'log', 'sign', 'create', 'account', 'log', 'facebook', 'connect', 'friends', 'family', 'people', 'share', 'photos', 'videos', 'send', 'messages', 'updates'], ['facebook', 'linkedin', 'billion', 'people', 'facebook', 'instagram', 'whatsapp', 'messenger', 'month', 'stay', 'connected', 'friends', 'family', 'discover', 'window', 'onload', 'function', 'parse', 'tracking', 'code', 'cookies', 'var', 'trk', 'bf', 'var', 'trkinfo', 'bf', 'var', 'cookies', 'document', 'cookie', 'split', 'var', 'cookies', 'length', 'cookies', 'indexof', 'trkcode', 'amp', 'amp', 'cookies', 'length', 'trk', 'cookies', 'substring', 'cookies', 'indexof', 'trkinfo', 'amp', 'amp', 'cookies', 'length', 'trkinfo', 'cookies', 'substring', 'window', 'location', 'protocol', 'sl', 'cookie', 'set', 'redirect', 'var', 'cookies', 'length', 'cookies', 'indexof', 'sl', 'amp', 'amp', 'cookies', 'length', 'window', 'location', 'href', 'window', 'location', 'href', 'substring', 'window', 'location', 'protocol', 'length', 'return', 'domain', 'international', 'domains', 'fr', 'linkedin', 'convert', 'www', 'linkedin', 'var', 'domain', 'www', 'linkedin', 'domain', 'location', 'host', 'var', 'subdomainindex', 'location', 'host', 'indexof', 'linkedin', 'subdomainindex', 'domain', 'www', 'location', 'host', 'substring', 'subdomainindex', 'window', 'location', 'href', 'domain', 'authwall', 'trk', 'trk', 'amp', 'trkinfo', 'trkinfo', 'amp', 'originalreferer', 'document', 'referrer', 'substr', 'amp', 'sessionredirect', 'encodeuricomponent', 'window', 'location', 'href'], ['top', 'facebook', 'statistics', 'updated', 'april', 'facebook', 'represents', 'huge', 'potential', 'market', 'social', 'media', 'efforts', 'increasingly', 'difficult', 'stand', 'crowd', 'error', 'code'], ['smart', 'video', 'calling', 'alexa', 'built-in', 'portal', 'facebook', 'feel', 'portal', 'facebook', 'move', 'talk', 'freely', 'smart', 'camera', 'portals', 'alexa', 'built-in']], 'dictionnaire': ['facebook', 'log', 'sign', 'create', 'account', 'connect', 'friends', 'family', 'people', 'share', 'photos', 'videos', 'send', 'messages', 'updates', 'start', 'sharing', 'connecting', 'developers', 'code', 'explore', 'business', 'tools', 'gaming', 'open', 'source', 'publishing', 'social', 'hardware', 'integration', 'ads', 'online', 'advertising', 'efficient', 'ways', 'advertise', 'businesses', 'device', 'marketing', 'messenger', 'instantly', 'life', 'started', 'continue', 'signed', 'analytics', 'drive', 'growth', 'web', 'mobile', 'tool', 'insights', 'understand', 'customers', 'journey', 'optimize', 'linkedin', 'billion', 'instagram', 'whatsapp', 'month', 'stay', 'connected', 'discover', 'top', 'statistics', 'updated', 'april', 'represents', 'huge', 'potential', 'market', 'media', 'efforts', 'increasingly', 'difficult', 'stand', 'crowd', 'smart', 'video', 'calling', 'alexa', 'built-in', 'portal', 'feel', 'move', 'talk', 'freely', 'camera', 'portals'], 'links': ['https://www.facebook.com/', 'https://www.facebook.com/login/', 'https://developers.facebook.com/', 'https://www.facebook.com/business/ads', 'https://m.facebook.com/home.php', 'https://en-gb.facebook.com/login/', 'https://en-gb.facebook.com/', 'https://www.messenger.com/login.php', 'https://analytics.facebook.com/', 'https://touch.facebook.com/', 'https://www.linkedin.com/company/facebook', 'https://zephoria.com/top-15-valuable-facebook-statistics/', 'https://portal.facebook.com/']}, 'selection': {'status': ['REJETE', 'REJETE', 'REJETE', 'REJETE', 'REJETE', 'REJETE', 'REJETE', 'REJETE', 'REJETE', 'REJETE', 'REJETE', 'REJETE', 'REPECHE', 'REJETE'], 'entities': [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', {'abs': [], 'eps': 0, 'score': 0, 'delta': 0, 'categories': [], 'name': u'Wikipedia talk:Requests for mediation/Danah Boyd - Wikipedia'}, ' '], 'selected': [11], 'score': {'content': [[0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0]], 'nb_colonnes': 8, 'nb_lignes': 13}, 'moteur': 'GOOGLE'}}"

import json
obj = json.loads(obj.replace("u'", "'").replace("'", "\""))

def main(args):
	corpus = args.get("corpus")
	selection = args.get("selection")
	corpus = args["corpus"]
	selection = args["selection"]
	
	extraction = extract(corpus, selection)
	
	return extraction["scores"]

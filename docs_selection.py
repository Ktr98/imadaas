import imad_text_search_functions as itsf

SEUIL_SELECTION = 75
NB_ENTITES_SELECTION = 10
SYLLABES_MIN = 2
STR_DISTANCE = "lcs"
MOTEUR = "GOOGLE"

def main(params):
    corpus = params["corpus"]
    app_name = params["app_name"]
    clustering = params["clustering"]
    analysis = params["analysis"]
    selection = itsf.select(corpus, clustering, analysis, app_name, SEUIL_SELECTION, NB_ENTITES_SELECTION, SYLLABES_MIN, STR_DISTANCE, MOTEUR)
    return {
        "selection": selection,
        "corpus": corpus
    }

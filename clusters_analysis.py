import imad_text_search_functions as itsf

NB_DESC = 5
NB_ENTITES_DESC = 3
SEUIL_TOPIC = 1
CLUSTERING = True
MOTEUR = "GOOGLE"

def main(params):
    corpus = params["corpus"]
    app_name = params["app_name"]
    keywords = params["keywords"]
    clustering = params["clustering"]
    analysis = itsf.analyse(corpus, keywords, clustering, NB_DESC, NB_ENTITES_DESC, SEUIL_TOPIC, CLUSTERING, MOTEUR)
    return {
        "corpus": corpus,
        "app_name": app_name,
        "clustering": clustering,
        "analysis": analysis
    }



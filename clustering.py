import math
import random
import copy

class Matrix(object):
	lignes = 0
	colonnes = 0
	content = []
	
	def __init__(self, lignes, colonnes):
		self.lignes = lignes
		self.colonnes = colonnes
		self.content = []
		for i in range(lignes):
			self.content.append([0]*colonnes)
	
	def __eq__(self, other):
		for i in range(self.lignes):
			for j in range(self.colonnes):
				if self.content[i][j] != other.get(i, j):
					return False
		return True
		
	def getNbLignes(self): return self.lignes
	def getNbColonnes(self): return self.colonnes
	
	def set(self, i, j, val): self.content[i][j] = val
	def get(self, i, j): return self.content[i][j]
	
	def getRow(self, i):
		row = []
		for j in range(self.colonnes): row.append(self.get(i, j))
		return row
		
	def setRow(self, i, row):
		for j in range(self.colonnes):
			self.set(i, j, row[j])
	
	def scaleCol(self, j, scalar):
		for i in range(self.lignes):
			self.content[i][j] *= scalar
			
	def scaleRow(self, i, scalar):
		for j in range(self.colonnes):
			self.content[i][j] *= scalar
			
	def scale(self, scalar):
		for i in range(self.lignes):
			for j in range(self.colonnes):
				self.set(i, j, self.get(i, j)*scalar)
	
	def maxCol(self):
		maxi = Matrix(1, self.colonnes)
		for j in range(self.colonnes):
			valMax = self.get(0, j)
			for i in range(1, self.lignes):
				if valMax < self.get(i, j):
					valMax = self.get(i, j)
			maxi.set(0, j, valMax)
		return maxi
		
	def maxRow(self, i):
		maxi = 0
		for j in range(self.colonnes):
			maxi = max(maxi, self.get(i, j))
		return maxi
	
	def concatenateCol(self, indices):
		nb_col = len(indices)
		result = Matrix(self.lignes, nb_col)
		for i in range(self.lignes):
			for j in range(self.colonnes):
				result.set(i, j, self.get(i, indices[j]))
		return result
		
	def sub(self, i, di, j, dj):
		result = Matrix(di, dj)
		for x in range(di):
			for y in range(dj):
				result.set(x, y, self.get(i+x, j+y))
		return result
	def sum(self):
		sumi = 0
		for i in range(self.lignes):
			for j in range(self.colonnes):
				sumi += self.get(i, j)
		return sumi
	def sumCol(self):
		summ = Matrix(1, self.colonnes)
		for j in range(self.colonnes):
			val = 0
			for i in range(self.lignes):
				val += self.get(i, j)
			summ.set(0, j, val)
		return summ
	def toDict(self):
		return  {
		    "nb_lignes": self.lignes,
		    "nb_colonnes": self.colonnes, 
		    "content": self.content
		}

def nb_occurences(mot, document):
	nb_occ = 0
	for m in document:
		if m == mot: nb_occ += 1
	return nb_occ

def vector_space(documents, dictionnaire, typ):
	NB_DOC = len(documents)
	NB_DICO = len(dictionnaire)
	
	tf_idf = Matrix(NB_DOC, NB_DICO)
	tf = Matrix(NB_DOC, NB_DICO)
	
	for i in range(NB_DICO):
		idf = 0
		for j in range(NB_DOC):
			nb_occ = nb_occurences(dictionnaire[i], documents[j])
			if nb_occ != 0:
				idf += 1
			f = nb_occ/len(documents[j])
			tf_idf.set(j, i, f)
			tf.set(j, i, f)
		idf = math.log(NB_DOC / idf)
		tf_idf.scaleCol(i, idf)
		
	if typ == 'tf-idf': return tf_idf
	elif typ == 'tf': return tf
	else: return [tf_idf, tf]

def get_keywords(corpus):
	documents = corpus["documents"]
	dictionnaire = corpus["dictionnaire"]

	keywords = {
			"tf_idf": 0,
			"tf": 0,
			"scores": [],
			"indices": []
		}
	
	NB_DICO = len(dictionnaire)
	
	result = vector_space(documents, dictionnaire, 'both')
	keywords["tf_idf"] = result[0]
	keywords["tf"] = result[1]
	
	keywords["scores"] = keywords["tf_idf"].maxCol()
	keywords["scores"].scale(-1)
	scores = keywords["scores"].getRow(0)
	keywords["scores"].scale(-1)
	
	keywords["indices"] = []
	for i in range(NB_DICO):
		keywords["indices"].append(i)
	l1, l2, l3 = zip(*sorted(zip(scores, keywords["indices"], dictionnaire)))
	scores = list(l1)
	keywords["indices"] = list(l2)
	dictionnaire = list(l3)
	
	keywords["tf_idf"] = keywords["tf_idf"].concatenateCol(keywords["indices"])
	keywords["tf"] = keywords["tf"].concatenateCol(keywords["indices"])
	
	return keywords

def tools_delete(v, elt):
	result = []
	for e in v:
		if e != elt: result.append(e)
	return result
	
def tools_dist_euclid(l1, l2, m):
	result = 0
	for i in range(m):
		result += (l1[i] - l2[i])**2
	return result**0.5

def kmeansPP(x, k):
	n = x.getNbLignes()
	m = x.getNbColonnes()
	c0 = Matrix(k, m)
	values = [0]*n
	for i in range(1, n):
		values[i] = i

	val = random.randint(0, n-1)
	c0.setRow(0, x.getRow(val))
	
	values = tools_delete(values, val)
	
	for i in range(1, k):
		dist = Matrix(1, n)
		for j in range(i):
			for p in values:
				dist.set(0, p, dist.get(0, p) + tools_dist_euclid(x.getRow(p), c0.getRow(j), m))
		summ = dist.sum()
		
		if summ != 0:
			dist.scale(100/summ)
		
		val =  random.randint(0, n-1)
		proba = 0
		for p in values:
			if proba != -1 and proba > val:
				proba = -1
				val = p
			else:
				proba += dist.get(0, p)

		c0.setRow(i, x.getRow(val))
		values = tools_delete(values, val)
		
	return c0

def kmeans_cluster(clustering, x, k, NB_ITMAX):
	n = x.getNbLignes()
	m = x.getNbColonnes()
	
	k_classes = Matrix(1, n)
	k_distances = Matrix(1, n)
	k_centres = kmeansPP(x, k)
	
	convergence = False
	it = 0
	while not(convergence) and it < NB_ITMAX:
		old_k_classes = copy.deepcopy(k_classes)
		for i in range(n):
			k_distances.set(0, i, -1)
			for p in range(k):
				distance = tools_dist_euclid(x.getRow(i), k_centres.getRow(p), m)
				if k_distances.get(0, i) == -1 or k_distances.get(0, i) > distance:
					k_distances.set(0, i, distance)
					k_classes.set(0, i, p)
		
		for p in range(k):
			k_centres.setRow(p, [0]*m)
			nb_points = 0
			for i in range(n):
				if k_classes.get(0, i) == p:
					for j in range(m):
						k_centres.set(p, j, k_centres.get(p, j) + x.get(i, j))
			
			if nb_points != 0:
				k_centres.scaleRow(p, 1/nb_points)
				
		convergence = old_k_classes == k_classes
		it += 1
	clustering["classes"] = k_classes
	clustering["distances"] = k_distances
	clustering["centres"] = k_centres

def description_cluster(clustering, tf):
	NB_DOC = tf.getNbLignes()
	NB_DICO = tf.getNbColonnes()
	k = clustering["k"]
	clustering["scores"] = Matrix(k, NB_DICO)
	clustering["indices"] = Matrix(k, NB_DICO)
	classes = clustering["classes"]
	
	tf.scale(-1)
	for i in range(k):
		mat = Matrix(NB_DOC, NB_DICO)
		for j in range(NB_DOC):
			if classes.get(0, j) == i:
				mat.setRow(j, tf.getRow(j))
		
		scores = mat.sumCol().getRow(0)
		indices = []
		for j in range(NB_DICO):
			indices.append(j)
		l1, l2 = zip(*sorted(zip(scores, indices)))
		scores = list(l1)
		indices = list(l2)
		clustering["scores"].setRow(i, scores)
		clustering["indices"].setRow(i, indices)
	tf.scale(-1)
	clustering["scores"].scale(-1)
	

def process_clustering(keywords, NB_KEYWORDS, NB_ITMAX, CLUSTERING):
	clustering = {}
	
	tf_idf = keywords["tf_idf"]
	tf = keywords["tf"]
	
	indices = keywords["indices"]
	
	NB_DOC = tf_idf.getNbLignes()
	NB_KEYWORDS = tf_idf.getNbColonnes()
	
	if CLUSTERING:
		t = 0
		for i in range(NB_DOC):
			for j in range(NB_KEYWORDS):
				if tf_idf.get(i, j) != 0:
					t+=1
		clustering["k"] = math.ceil( (NB_DOC * NB_KEYWORDS) / max(1, t))
		x = tf_idf.sub(0, NB_DOC, 0, NB_KEYWORDS)
		kmeans_cluster(clustering, x, clustering["k"], NB_ITMAX)
		description_cluster(clustering, tf)
	else:
		clustering = {
			"k": 1,
			"classes": Matrix(1, NB_DOC),
			"distances": Matrix(1, NB_DOC),
			"centres": Matrix(1, NB_KEYWORDS),
			"scores": Matrix(1, NB_KEYWORDS),
			"indices": Matrix(1, NB_KEYWORDS),
		}
		
	return clustering

obj = "{'corpus': {'titres': [[u'facebook', u'log', u'sign'], [u'log', u'facebook', u'facebook'], [u'facebook', u'developers'], [u'facebook', u'ads', u'online', u'advertising', u'facebook', u'facebook'], [u'log', u'facebook', u'facebook'], [u'log', u'facebook', u'facebook'], [u'facebook', u'log', u'sign'], [u'facebook'], [u'facebook', u'analytics', u'drive', u'growth', u'web', u'mobile'], [u'facebook', u'log', u'sign'], [u'facebook', u'linkedin'], [u'top', u'facebook', u'statistics', u'updated', u'april'], [u'smart', u'video', u'calling', u'alexa', u'built-in', u'portal', u'facebook']], 'documents': [[u'facebook', u'log', u'sign', u'create', u'account', u'log', u'facebook', u'connect', u'friends', u'family', u'people', u'share', u'photos', u'videos', u'send', u'messages', u'updates'], [u'log', u'facebook', u'facebook', u'log', u'facebook', u'start', u'sharing', u'connecting', u'friends', u'family', u'people'], [u'facebook', u'developers', u'code', u'connect', u'people', u'facebook', u'developers', u'explore', u'ai', u'business', u'tools', u'gaming', u'open', u'source', u'publishing', u'social', u'hardware', u'social', u'integration'], [u'facebook', u'ads', u'online', u'advertising', u'facebook', u'facebook', u'facebook', u'efficient', u'ways', u'advertise', u'online', u'connect', u'businesses', u'people', u'device', u'facebook', u'marketing'], [u'log', u'facebook', u'facebook', u'log', u'facebook', u'start', u'sharing', u'connecting', u'friends', u'family', u'people'], [u'log', u'facebook', u'facebook', u'log', u'facebook', u'start', u'sharing', u'connecting', u'friends', u'family', u'people'], [u'facebook', u'log', u'sign', u'create', u'account', u'log', u'facebook', u'connect', u'friends', u'family', u'people', u'share', u'photos', u'videos', u'send', u'messages', u'updates'], [u'facebook', u'messenger', u'instantly', u'connect', u'people', u'life', u'sign', u'facebook', u'started', u'continue', u'signed', u'facebook'], [u'facebook', u'analytics', u'drive', u'growth', u'web', u'mobile', u'facebook', u'analytics', u'tool', u'insights', u'business', u'understand', u'customers', u'journey', u'mobile', u'web', u'optimize', u'growth'], [u'facebook', u'log', u'sign', u'create', u'account', u'log', u'facebook', u'connect', u'friends', u'family', u'people', u'share', u'photos', u'videos', u'send', u'messages', u'updates'], [u'facebook', u'linkedin', u'billion', u'people', u'facebook', u'instagram', u'whatsapp', u'messenger', u'month', u'stay', u'connected', u'friends', u'family', u'discover', 'window', 'onload', 'function', 'parse', 'tracking', 'code', 'cookies', 'var', 'trk', 'bf', 'var', 'trkinfo', 'bf', 'var', 'cookies', 'document', 'cookie', 'split', 'var', 'cookies', 'length', 'cookies', 'indexof', 'trkcode', 'amp', 'amp', 'cookies', 'length', 'trk', 'cookies', 'substring', 'cookies', 'indexof', 'trkinfo', 'amp', 'amp', 'cookies', 'length', 'trkinfo', 'cookies', 'substring', 'window', 'location', 'protocol', 'sl', 'cookie', 'set', 'redirect', 'var', 'cookies', 'length', 'cookies', 'indexof', 'sl', 'amp', 'amp', 'cookies', 'length', 'window', 'location', 'href', 'window', 'location', 'href', 'substring', 'window', 'location', 'protocol', 'length', 'return', 'domain', 'international', 'domains', 'fr', 'linkedin', 'convert', 'www', 'linkedin', 'var', 'domain', 'www', 'linkedin', 'domain', 'location', 'host', 'var', 'subdomainindex', 'location', 'host', 'indexof', 'linkedin', 'subdomainindex', 'domain', 'www', 'location', 'host', 'substring', 'subdomainindex', 'window', 'location', 'href', 'domain', 'authwall', 'trk', 'trk', 'amp', 'trkinfo', 'trkinfo', 'amp', 'originalreferer', 'document', 'referrer', 'substr', 'amp', 'sessionredirect', 'encodeuricomponent', 'window', 'location', 'href'], [u'top', u'facebook', u'statistics', u'updated', u'april', u'facebook', u'represents', u'huge', u'potential', u'market', u'social', u'media', u'efforts', u'increasingly', u'difficult', u'stand', u'crowd', 'error', 'code'], [u'smart', u'video', u'calling', u'alexa', u'built-in', u'portal', u'facebook', u'feel', u'portal', u'facebook', u'move', u'talk', u'freely', u'smart', u'camera', u'portals', u'alexa', u'built-in']], 'dictionnaire': [u'facebook', u'log', u'sign', u'create', u'account', u'connect', u'friends', u'family', u'people', u'share', u'photos', u'videos', u'send', u'messages', u'updates', u'start', u'sharing', u'connecting', u'developers', u'code', u'explore', u'business', u'tools', u'gaming', u'open', u'source', u'publishing', u'social', u'hardware', u'integration', u'ads', u'online', u'advertising', u'efficient', u'ways', u'advertise', u'businesses', u'device', u'marketing', u'messenger', u'instantly', u'life', u'started', u'continue', u'signed', u'analytics', u'drive', u'growth', u'web', u'mobile', u'tool', u'insights', u'understand', u'customers', u'journey', u'optimize', u'linkedin', u'billion', u'instagram', u'whatsapp', u'month', u'stay', u'connected', u'discover', u'top', u'statistics', u'updated', u'april', u'represents', u'huge', u'potential', u'market', u'media', u'efforts', u'increasingly', u'difficult', u'stand', u'crowd', u'smart', u'video', u'calling', u'alexa', u'built-in', u'portal', u'feel', u'move', u'talk', u'freely', u'camera', u'portals'], 'links': [u'https://www.facebook.com/', u'https://www.facebook.com/login/', u'https://developers.facebook.com/', u'https://www.facebook.com/business/ads', u'https://m.facebook.com/home.php', u'https://en-gb.facebook.com/login/', u'https://en-gb.facebook.com/', u'https://www.messenger.com/login.php', u'https://analytics.facebook.com/', u'https://touch.facebook.com/', u'https://www.linkedin.com/company/facebook', u'https://zephoria.com/top-15-valuable-facebook-statistics/', u'https://portal.facebook.com/']}, 'app_name': 'facebook'}"

import json
obj = json.loads(obj.replace("u'", "'").replace("'", "\""))

def main(args):
	#corpus = args["corpus"]
	#app_name = args["app_name"]
	corpus = args.get("corpus")
	app_name = args.get("app_name")

	keywords = get_keywords(corpus)
	clustering = process_clustering(keywords, 50, 100, True)
	
	
	keywords["tf"] = keywords["tf"].toDict()
	keywords["tf_idf"] = keywords["tf_idf"].toDict()
	keywords["scores"] = keywords["scores"].toDict()
	
	clustering["classes"] = clustering["classes"].toDict()
	clustering["distances"] = clustering["distances"].toDict()
	clustering["centres"] = clustering["centres"].toDict()
	clustering["scores"] = clustering["scores"].toDict()
	clustering["indices"] = clustering["indices"].toDict()
	
	return {
		"corpus": corpus,
		"keywords": keywords,
		"clustering": clustering,
		"app_name": app_name
	}

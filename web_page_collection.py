import imad_text_search_functions as itsf
import re

NB_MOTS_MIN = 1
NB_MOTS_MAX = 2000
TAILLE_MOT_MIN = 3
NBRE_DIZAINES_RESULTATS = 2
MOTEUR = "BINK"

def main(args):
    #app_name = args.get("app_name")
    app_name = re.sub(" ", "+" ,args)
    app_name = re.sub("\n", "" ,app_name)
    corpus = itsf.build_corpus(app_name, NB_MOTS_MIN, NB_MOTS_MAX, TAILLE_MOT_MIN, NBRE_DIZAINES_RESULTATS, MOTEUR)
    return {"corpus" : corpus, "app_name": app_name}





   

